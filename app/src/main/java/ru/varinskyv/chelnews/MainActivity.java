package ru.varinskyv.chelnews;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;

import ru.varinskyv.chelnews.adapters.NewsCollectionPagerAdapter;
import ru.varinskyv.chelnews.caches.NewsListCache;
import ru.varinskyv.chelnews.data.NewsTab;
import ru.varinskyv.chelnews.utils.ImageLoader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NewsListCache.getInstance();
        ImageLoader.getInstance().init(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        ArrayList<NewsTab> tabsList = new ArrayList<>();
        tabsList.add(new NewsTab("Главные темы", "/chel/", "flL1"));
        tabsList.add(new NewsTab("Новости дня", "/chel/today/", "flR1"));
        tabsList.add(new NewsTab("Простыми словами", "/chel/lectures/", "flR1"));
        tabsList.add(new NewsTab("Анонсы", "/chel/announc/", "flR1"));

        NewsCollectionPagerAdapter newsCollectionPagerAdapter =
                new NewsCollectionPagerAdapter(
                        getSupportFragmentManager(), tabsList);
        ViewPager newsViewPager = (ViewPager) findViewById(R.id.main_news_pager);
        newsViewPager.setAdapter(newsCollectionPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_main);
        tabLayout.setupWithViewPager(newsViewPager);
    }

}
