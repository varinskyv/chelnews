package ru.varinskyv.chelnews.caches;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

import ru.varinskyv.chelnews.utils.ImageRequest;

public class ImageDiscCache {

    private static final String TAG = "ImageDiscCache";

    private static File discCacheDir;

    private ImageDiscCache(Context context, String cacheDir) {
        ImageDiscCache.discCacheDir = getDiskCacheDir(context, cacheDir);

        Log.d(TAG, String.format("Disk cache dir = %s", cacheDir));
        Log.d(TAG, String.format("Total memory = %s Kb",
                (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }

    public static ImageDiscCache init(Context context, String diskCacheDir) {
        return new ImageDiscCache(context, diskCacheDir);
    }

    private static File getDiskCacheDir(Context context, String uniqueName) {
        final String cachePath = context.getCacheDir().getPath();

        File result = new File(cachePath + File.separator + uniqueName);

        if (!result.exists()) {
            if (!result.mkdir())
                result = new File(cachePath);
        }

        return result;
    }

    public static void getBitmapFromDiskCache(String url, ImageView imageView, ImageRequest.OnLoadImage onLoadImage) {
        new LoadBitmap(imageView, onLoadImage).execute(url);
    }

    public void addBitmapToDiskCache(String url, Bitmap bitmap) {
        new SaveBitmap(bitmap).execute(url);
    }

    private static Bitmap getBitmap(String url) {
        Bitmap bitmap = null;

        String[] temp = url.split(File.separator);
        String fileName = temp[temp.length - 1];

        File bitmapFile = new File(discCacheDir, fileName);

        if (bitmapFile.exists()) {
            try {
                bitmap = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    public static class LoadBitmap extends AsyncTask<String, Void, Bitmap> {

        private final ImageView imageView;
        private final ImageRequest.OnLoadImage onLoadImage;
        private String url;

        public LoadBitmap(ImageView imageView, ImageRequest.OnLoadImage onLoadImage) {
            this.imageView = imageView;
            this.onLoadImage = onLoadImage;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            url = params[0];

//            String[] temp = url.split(File.separator);
//            String fileName = temp[temp.length - 1];
//
//            Bitmap bitmap = null;
//            File bitmapFile = new File(discCacheDir, fileName);
//
//            try {
//                bitmap = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            return getBitmap(url);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (imageView != null) {
                if (result != null) {
                    imageView.setImageBitmap(result);

                    Log.d(TAG, String.format("Load bitmap from disk cache = %s", url));

                    if (onLoadImage != null)
                        onLoadImage.onLoadImage(url, result, false);
                }
                else {
                    new ImageRequest(imageView, onLoadImage).execute(url);
                }
            }
        }
    }

    public static class SaveBitmap extends AsyncTask<String, Void, Void> {

        private final Bitmap bitmap;

        public SaveBitmap(Bitmap bitmap) {
            this.bitmap = bitmap;
        }

        @Override
        protected Void doInBackground(String... params) {
            Bitmap loadedBitmap = getBitmap(params[0]);

            if (loadedBitmap == null) {
                String[] temp = params[0].split(File.separator);
                String fileName = temp[temp.length - 1];

                File bitmapFile = new File(discCacheDir, fileName);

                try {
//                loadedBitmap = BitmapFactory.decodeFile(bitmapFile.getAbsolutePath());

//                    if (loadedBitmap == null) {
                    FileOutputStream fileOutputStream = new FileOutputStream(bitmapFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    fileOutputStream.flush();
                    fileOutputStream.close();

                    Log.d(TAG, String.format("Saved file = %s", bitmapFile.getAbsoluteFile()));
                    Log.d(TAG, String.format("Total memory = %s Kb",
                            (int) (Runtime.getRuntime().totalMemory() / 1024)));
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }

}
