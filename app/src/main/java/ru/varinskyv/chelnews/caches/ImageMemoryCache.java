package ru.varinskyv.chelnews.caches;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.util.Log;


public class ImageMemoryCache {

    private static final String TAG = "ImageMemoryCache";

    //резервный размер
    private static final int CACHE_SIZE = 1024 * 4; //4 Mb

    private final LruCache<String, Bitmap> memoryCache;

    private ImageMemoryCache(int cacheSize) {
        memoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };

        Log.d(TAG, String.format("Total memory = %s Kb, Cache memory = %s Kb",
                (int) (Runtime.getRuntime().totalMemory() / 1024), cacheSize));
    }

    public static ImageMemoryCache init (int cacheSize) {
        if (cacheSize <= 0)
            return new ImageMemoryCache(CACHE_SIZE);
        else
            return new ImageMemoryCache(cacheSize);
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null && bitmap != null) {
            memoryCache.put(key, bitmap);

            Log.d(TAG, String.format("Added Key = %s", key));
        }

        Log.d(TAG, String.format("Total memory = %s Kb",
                (int) (Runtime.getRuntime().totalMemory() / 1024)));
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }

}
