package ru.varinskyv.chelnews.caches;

import java.util.ArrayList;

import ru.varinskyv.chelnews.data.News;
import ru.varinskyv.chelnews.data.NewsList;

public class NewsListCache {

    private static NewsListCache ourInstance = new NewsListCache();

    private ArrayList<NewsList> newsListCache;

    public static NewsListCache getInstance() {
        return ourInstance;
    }

    private NewsListCache() {
        newsListCache = new  ArrayList<>();
    }

    public void putNewsList(int index, ArrayList<News> newsList) {
        NewsList cacheElement = new NewsList();
        cacheElement.index = index;
        cacheElement.newsList = newsList;

        if (newsListCache.indexOf(cacheElement) < 0)
            newsListCache.add(cacheElement);
        else {
            newsListCache.remove(newsListCache.indexOf(cacheElement));
            newsListCache.add(cacheElement);
        }
    }

    public ArrayList<News> getNewsList(int index) {
        NewsList cacheElement = new NewsList();
        cacheElement.index = index;

        if (newsListCache.indexOf(cacheElement) < 0)
            return new ArrayList<>();

        return newsListCache.get(newsListCache.indexOf(cacheElement)).newsList;
    }

}
