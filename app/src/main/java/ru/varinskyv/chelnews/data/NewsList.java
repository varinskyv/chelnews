package ru.varinskyv.chelnews.data;

import java.util.ArrayList;

public class NewsList {

    public int index;
    public ArrayList<News> newsList;

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (this == o)
            return true;

        if (index == ((NewsList) o).index)
            return true;

        return false;
    }

}
