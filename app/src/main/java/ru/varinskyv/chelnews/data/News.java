package ru.varinskyv.chelnews.data;

import org.jsoup.select.Elements;

public class News {

    public String imageUrl = "";
    public String title = "";
    public String annotation = "";
    public String description = "";
    public String newsUrl = "";
    public String datePublished = "";
    public boolean isHeader;

    public String htmlDocument = "";

    public News(boolean isHeader) {
        this.isHeader = isHeader;
    }

    public void parse(Elements section) {

        Elements temp = section.select("a");
        if (temp.size() > 0)
            newsUrl = temp.first().attr("href");

        temp = section.select("img");
        if (temp.size() > 0)
            imageUrl = temp.first().attr("src");

        temp = section.select("strong");
        if (temp.size() > 0)
            title = temp.first().html();

        temp = section.select("em");
        if (temp.size() > 0)
            annotation = temp.first().html();

        temp = section.select("p[itemprop=description]");
        if (temp.size() > 0)
            description = temp.first().html();

        temp = section.select("meta[itemprop=datePublished]");
        if (temp.size() > 0)
            datePublished = temp.first().attr("content");

    }

    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;

        if (this == o)
            return true;

        if (newsUrl.equals(((News) o).newsUrl))
            return true;

        return false;
    }
}
