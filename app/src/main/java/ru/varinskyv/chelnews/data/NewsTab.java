package ru.varinskyv.chelnews.data;

import java.io.Serializable;

public class NewsTab implements Serializable {

    public String title;
    public String url;
    public String tag;

    public NewsTab() {
        title = "";
        url = "";
        tag = "";
    }

    public NewsTab(String title, String url, String tag) {
        this.title = title;
        this.url = url;
        this.tag = tag;
    }

}
