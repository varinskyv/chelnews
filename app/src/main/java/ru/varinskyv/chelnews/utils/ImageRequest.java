package ru.varinskyv.chelnews.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;
import java.net.URL;

public class ImageRequest extends AsyncTask<String, Void, Bitmap> {

    private static final String TAG = "ImageRequest";

    private final ImageView imageView;
    private final OnLoadImage onLoadImage;
    private String url;

    public interface OnLoadImage {
        public void onLoadImage(String url, Bitmap bitmap, boolean isNeedDiskCache);
    }

    public ImageRequest(ImageView imageView, OnLoadImage onLoadImage) {
        this.imageView = imageView;
        this.onLoadImage = onLoadImage;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        url = params[0];

        Bitmap bitmap = null;

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(new URL(params[0]).openStream(),
                    new Rect(0,0,0,0), options);

            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inJustDecodeBounds = false;

            bitmap = BitmapFactory.decodeStream(new URL(params[0]).openStream(),
                    new Rect(0,0,0,0), options);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, String.format("Load bitmap from internet = %s", url));

        if (bitmap != null)
            Log.d(TAG, String.format("Total memory = %s Kb, File size = %s Kb",
                    (int) (Runtime.getRuntime().totalMemory() / 1024),
                    (bitmap.getWidth() * bitmap.getHeight() * 4) / 1024));
        else
            Log.d(TAG, String.format("Total memory = %s Kb, File size = null",
                    (int) (Runtime.getRuntime().totalMemory() / 1024)));

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result)
    {
        if (imageView != null)
            imageView.setImageBitmap(result);

        if (onLoadImage != null)
            onLoadImage.onLoadImage(url, result, true);
    }
}
