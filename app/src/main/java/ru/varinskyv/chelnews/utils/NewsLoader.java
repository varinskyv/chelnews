package ru.varinskyv.chelnews.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.view.View;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import ru.varinskyv.chelnews.caches.NewsListCache;
import ru.varinskyv.chelnews.data.News;
import ru.varinskyv.chelnews.data.NewsTab;

public class NewsLoader implements HTTPRequest.OnGetRequestAsyncTaskListener {

    private final static int LOAD_NEWS_BODY = -1;

    private final Context context;
    private OnLoadNewsList onLoadNewsList;
    private OnLoadNews onLoadNews;
    private ArrayList<News> newsList = new ArrayList<>();
    private int actionType;

    public NewsListCache newsListCache;

    public interface OnLoadNewsList {
        public void onNewsListLoadDone(ArrayList<News> result);
        public void onNewsListLoadError();
    }

    public interface OnLoadNews {
        public void onNewsLoadDone(News result);
        public void onNewsLoadError();
    }

    public NewsLoader(Context context) {
        this.context = context;

        newsListCache = NewsListCache.getInstance();
    }

    public void setOnLoadNewsListListener(OnLoadNewsList onLoadNewsList) {
        this.onLoadNewsList = onLoadNewsList;
    }

    public void setOnLoadNewsListener(OnLoadNews onLoadNews) {
        this.onLoadNews = onLoadNews;
    }

    public void loadNewsList(int tabPosition, NewsTab newsTab) {
        actionType = tabPosition;

        startRequest(newsTab.url, String.format("div.%s", newsTab.tag));
    }

    public void loadNews(String newsUrl) {
        actionType = LOAD_NEWS_BODY;

        startRequest(newsUrl, "div[class=ablkL]");
    }

    private void startRequest(String url, String tag)
    {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected())
        {
            String[] addresses = new String[2];
            addresses[0] = Constants.HOST_URL;
            addresses[1] = url;

            new HTTPRequest(tag, this).execute(addresses);
        }
        else
        {
            onLoadNewsList.onNewsListLoadError();

            makeSnackbar("Отсутствует интернет", "Настройка",
                    snackbarOnClickListenerActionSettings);
        }
    }

    @Override
    public void onGetRequestAsyncTaskResult(Elements result) {
        if (result != null) {
            switch (actionType) {
                case LOAD_NEWS_BODY: new NewsBody(result).execute(); break;

                default: new NewsListParser(actionType, result).execute();
            }
        }
        else {
            switch (actionType) {
                case LOAD_NEWS_BODY: onLoadNews.onNewsLoadError();

                default: onLoadNewsList.onNewsListLoadError();
            }

            makeSnackbar("Ошибка загрузки новостей", null, null);
        }
    }

    private void makeSnackbar(String title, String actionTitle,
                              View.OnClickListener onClickListener) {
        View rootView = ((Activity)context).getWindow().getDecorView()
                .findViewById(android.R.id.content);

        if (actionTitle != null && onClickListener != null) {
            Snackbar.make(rootView, title,
                    Snackbar.LENGTH_LONG).setAction(actionTitle, onClickListener).show();
        }
        else {
            Snackbar.make(rootView, title, Snackbar.LENGTH_LONG).show();
        }
    }

    private View.OnClickListener snackbarOnClickListenerActionSettings = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            context.startActivity(new Intent(Settings.ACTION_SETTINGS));
        }
    };

    private class NewsListParser extends AsyncTask<Void, Void, ArrayList<News>> {

        private final Elements htmlTag;
        private final int tabPosition;

        public NewsListParser(int tabPosition, Elements htmlTag) {
            this.tabPosition = tabPosition;
            this.htmlTag = htmlTag;
        }

        @Override
        protected ArrayList<News> doInBackground(Void... params) {
            ArrayList<News> result = new ArrayList<>();

            for (Element tagItem: htmlTag) {
                Elements section = tagItem.select("section");

                if (section.size() > 0) {
                    News news = new News(false);
                    news.parse(section);

                    if (result.indexOf(news) < 0 && newsList.indexOf(news) < 0) {
                        result.add(news);
                    }
                    else
                    {
                        if (newsList.indexOf(news) == 0)
                            break;
                    }
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<News> result)
        {
            if (tabPosition == 0) {
                if (result.size() > 5)
                    newsList.addAll(0, result.subList(0, 5));
                else
                    newsList.addAll(0, result);
            }
            else {
                if (newsList.size() > 1)
                    newsList.addAll(0, result);
                else {
                    if (result.size() > 9)
                        newsList.addAll(0, result.subList(0, 9));
                    else
                        newsList.addAll(0, result);
                }
            }

            newsListCache.putNewsList(tabPosition, newsList);

            onLoadNewsList.onNewsListLoadDone(newsList);
        }

    }

    private class NewsBody extends AsyncTask<Void, Void, News> {

        private final Elements htmlTag;

        public NewsBody(Elements htmlTag) {
            this.htmlTag = htmlTag;
        }

        @Override
        protected News doInBackground(Void... params) {
            News news = new News(false);

            String htmlTemplate = getTemplateFromAssetFile("temp2.html");
            String head = getTemplateFromAssetFile("head2.html");

            String body = htmlTag.html();
            //body = body.replace(city, Constants.HOST_URL + city);

            news.htmlDocument = String.format(htmlTemplate, head, body);

            return news;
        }

        private String getTemplateFromAssetFile(String filaName)
        {
            byte[] buffer = null;
            InputStream is;
            try {
                is = context.getAssets().open(filaName);
                int size = is.available();
                buffer = new byte[size];
                is.read(buffer);
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String str_data = new String(buffer);
            return str_data;
        }

        @Override
        protected void onPostExecute(News result)
        {
            onLoadNews.onNewsLoadDone(result);
        }

    }

}
