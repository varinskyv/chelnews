package ru.varinskyv.chelnews.utils;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class HTTPRequest extends AsyncTask<String, Void, Elements>
{

    public interface OnGetRequestAsyncTaskListener
    {
        public void onGetRequestAsyncTaskResult(Elements result);
    }

    private String findTag = null;
    private OnGetRequestAsyncTaskListener onGetRequestAsyncTaskListener;

    public HTTPRequest(String findTag,
                       OnGetRequestAsyncTaskListener onGetRequestAsyncTaskListener)
    {
        this.findTag = findTag;
        this.onGetRequestAsyncTaskListener = onGetRequestAsyncTaskListener;
    }

    @Override
    protected Elements doInBackground(String... addresses)
    {
        Elements result = null;
        try
        {
            if (addresses.length < 1 || findTag == null)
                return null;

            if (findTag.equals(""))
                return null;

            String urlString = "";
            for (String address: addresses)
                urlString += address;

            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(10000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            try
            {
                InputStream is = new BufferedInputStream(urlConnection.getInputStream());

                result = Jsoup.parse(is, "cp1251", urlString).select(findTag);

                is.close();
            }
            finally
            {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(Elements result)
    {
        onGetRequestAsyncTaskListener.onGetRequestAsyncTaskResult(result);
    }
}
