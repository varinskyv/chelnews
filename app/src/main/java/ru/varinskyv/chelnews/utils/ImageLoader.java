package ru.varinskyv.chelnews.utils;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import ru.varinskyv.chelnews.MainActivity;
import ru.varinskyv.chelnews.caches.ImageDiscCache;
import ru.varinskyv.chelnews.caches.ImageMemoryCache;

public class ImageLoader implements ImageRequest.OnLoadImage {

    private static final String TAG = "ImageLoader";

    private static ImageLoader instance = new ImageLoader();
    private static ImageMemoryCache imageMemoryCache;
    private static ImageDiscCache imageDiscCache;
    public static boolean isInit;

    public static ImageLoader getInstance() {
        return instance;
    }

    private ImageLoader() {}

    public static void init(MainActivity context) {
        if (!isInit) {
            int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
            imageMemoryCache = ImageMemoryCache.init(maxMemory / 8);

            imageDiscCache = ImageDiscCache.init(context, "img_cache");

            isInit = true;
        }
    }

    public void loadBitmap(String url, ImageView imageView) {
        imageView.setImageBitmap(null);
        Bitmap bitmap = imageMemoryCache.getBitmapFromMemCache(url);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);

            Log.d(TAG, String.format("Load bitmap from memory cache = %s", url));
        } else {
            imageDiscCache.getBitmapFromDiskCache(url, imageView, this);
        }
    }

    @Override
    public void onLoadImage(String url, Bitmap bitmap, boolean isNeedDiskCache) {
        Log.d(TAG, String.format("Start add to cache = %s\nisNeedDiskCache = %s",
                url, String.valueOf(isNeedDiskCache)));

        imageMemoryCache.addBitmapToMemoryCache(url, bitmap);

        if (isNeedDiskCache)
            imageDiscCache.addBitmapToDiskCache(url, bitmap);
    }

}
