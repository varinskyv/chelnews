package ru.varinskyv.chelnews.utils;

public class Constants {

    public static final String HOST_URL = "http://urfo.org";

    public static final String NEWS_URL = "NEWS_URL";
    public static final String CITY_URL = "CITY_URL";

    public static final String HTML_TEMPLATE = "<html lang=\"ru\">" +
            "<head>%s</head>" +
            "<body>%s</body></html>";
}
