package ru.varinskyv.chelnews;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import ru.varinskyv.chelnews.data.News;
import ru.varinskyv.chelnews.utils.Constants;
import ru.varinskyv.chelnews.utils.NewsLoader;

public class NewsActivity extends AppCompatActivity implements NewsLoader.OnLoadNews {

    private WebView webView;
    private RelativeLayout splash;
    private ArrayList<News> newsStack = new ArrayList<>();
    private String city;
    private NewsLoader newsLoader;
    private String newsUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_news);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.news_screen);
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("cp1251");
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        webView.setInitialScale(1);

        splash = (RelativeLayout) findViewById(R.id.news_splash);
        splash.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        city = intent.getStringExtra(Constants.CITY_URL);
        newsUrl = intent.getStringExtra(Constants.NEWS_URL);
        webView.setWebViewClient(new CustomWebViewClient(Constants.HOST_URL + city));
        webView.setVisibility(View.INVISIBLE);

        newsLoader = new NewsLoader(this);
        newsLoader.setOnLoadNewsListener(this);
        newsLoader.loadNews(newsUrl);
    }

    @Override
    public void onBackPressed() {
        if (newsStack.size() > 1) {
            showNews(newsStack.get(newsStack.size() - 2));
            newsStack.remove(newsStack.size() - 1);
        } else {
            super.onBackPressed();
        }
    }

    private void showNews(News news) {
        //String htmlTemplate = getTemplateFromAssetFile("temp.html");
        //String head = getTemplateFromAssetFile("head.html");
        //String body = news.htmlDocument;
        //body = body.replace(city, Constants.HOST_URL + city);

        //webView.loadDataWithBaseURL(null, String.format(htmlTemplate, head, body),
        //        "text/html", "cp1251", null);

        webView.loadDataWithBaseURL(null, news.htmlDocument, "text/html", "cp1251", null);
    }

    private String getTemplateFromAssetFile(String filaName)
    {
        byte[] buffer = null;
        InputStream is;
        try {
            is = getAssets().open(filaName);
            int size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String str_data = new String(buffer);
        return str_data;
    }

    @Override
    public void onNewsLoadDone(News result) {
        if (result != null) {
            showNews(result);
        }
    }

    @Override
    public void onNewsLoadError() {

    }

    private class CustomWebViewClient extends WebViewClient {

        private final String urlWithCity;

        public CustomWebViewClient(String urlWithCity) {
            this.urlWithCity = urlWithCity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.startsWith(urlWithCity)) {
                if (!url.equals(urlWithCity))
                    newsLoader.loadNews(url);
            }
            else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webView.setVisibility(View.VISIBLE);
            splash.setVisibility(View.GONE);
        }

    }

}
