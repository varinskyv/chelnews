package ru.varinskyv.chelnews.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import ru.varinskyv.chelnews.data.NewsTab;
import ru.varinskyv.chelnews.fragments.NewsListFragment;

public class NewsCollectionPagerAdapter extends FragmentStatePagerAdapter {

    private final ArrayList<NewsTab> newsTabs;

    public NewsCollectionPagerAdapter(FragmentManager fm,
                                      ArrayList<NewsTab> newsTabs) {
        super(fm);

        this.newsTabs = newsTabs;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new NewsListFragment();
        Bundle args = new Bundle();
        args.putInt(NewsListFragment.TAB_POSITION, position);
        args.putSerializable(NewsListFragment.NEWS_TAB, newsTabs.get(position));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return newsTabs.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return newsTabs.get(position).title;
    }

}
