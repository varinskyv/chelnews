package ru.varinskyv.chelnews.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.varinskyv.chelnews.R;

public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TAG = "NewsViewHolder";

    public interface OnItemClick {
        public void onItemClick(int position);
    }

    public final CardView newsContainer;
    public final RelativeLayout newsImageContainer;
    public final ImageView newsImage;
    public final RelativeLayout newsNewsContainer;
    public final TextView newsTitle;
    public final TextView newsAnnotation;
    public final RelativeLayout newsTopNewsContainer;
    public final RelativeLayout newsTopNewsImageContainer;
    public final ImageView newsTopNewsImage;
    public final TextView newsTitleTopNews;
    public final TextView newsAnnotationTopNews;
    public final TextView newsDescriptionTopNews;

    private final OnItemClick onItemClick;

    public NewsViewHolder(View itemView, Context context, OnItemClick onItemClick) {
        super(itemView);

        newsContainer = (CardView) itemView.findViewById(R.id.news_list_item_container);

        newsImageContainer = (RelativeLayout) itemView
                .findViewById(R.id.news_list_item_image_container);
        newsImage = (ImageView) itemView.findViewById(R.id.news_list_item_image);

        newsNewsContainer = (RelativeLayout) itemView
                .findViewById(R.id.news_list_item_news_container);
        newsTitle = (TextView) itemView.findViewById(R.id.news_list_item_title);
        newsAnnotation = (TextView) itemView.findViewById(R.id.news_list_item_annotation);

        newsTopNewsContainer = (RelativeLayout) itemView
                .findViewById(R.id.news_list_item_top_news_container);
        newsTopNewsImageContainer = (RelativeLayout) itemView
                .findViewById(R.id.news_list_item_top_news_image_container);
        newsTopNewsImage = (ImageView) itemView.findViewById(R.id.news_list_item_top_news_image);
        newsTitleTopNews = (TextView) itemView.findViewById(R.id.news_list_item_title_top_news);
        newsAnnotationTopNews = (TextView) itemView
                .findViewById(R.id.news_list_item_annotation_top_news);
        newsDescriptionTopNews = (TextView) itemView
                .findViewById(R.id.news_list_item_description_top_news);

        itemView.setOnClickListener(this);
        this.onItemClick = onItemClick;
    }

    @Override
    public void onClick(View v) {
        int position = getLayoutPosition();

        Log.d(TAG, String.format("Select item = %s", position));

        onItemClick.onItemClick(position);
    }

}
