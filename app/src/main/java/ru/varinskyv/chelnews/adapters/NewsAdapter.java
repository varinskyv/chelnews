package ru.varinskyv.chelnews.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ru.varinskyv.chelnews.R;
import ru.varinskyv.chelnews.data.News;
import ru.varinskyv.chelnews.utils.ImageLoader;

public class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> implements NewsViewHolder.OnItemClick {
    private static final String TAG = "NewsAdapter";

    public interface OnItemClick {
        public void onItemClick(String newsUrl);
        public void onHeaderClick();
    }

    private final Context context;
    private final int tabPosition;
    private final List<News> newsList;

    private final OnItemClick onItemClick;

    private final ImageLoader imageLoader;

    public boolean hasHeader;

    public NewsAdapter(Context context, int tabPosition, List<News> newsList, OnItemClick onItemClick) {
        this.context = context;
        this.tabPosition = tabPosition;
        this.newsList = newsList;
        this.onItemClick = onItemClick;

        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_item, parent, false);

        return new NewsViewHolder(v, context, this);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        if (position == 0) {
            hasHeader = newsList.get(position).isHeader;

            if (hasHeader)
                bindHeader(holder);
            else {
                if (tabPosition == 0)
                    bindTopNews(holder, position);
                else
                    bindNews(holder, position);
            }
        }
        else
            bindNews(holder, position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    private void bindNews(NewsViewHolder holder, int position) {
        holder.newsTopNewsContainer.setVisibility(View.GONE);
        holder.newsNewsContainer.setVisibility(View.VISIBLE);

        if (newsList.get(position).title.equals("")) {
            holder.newsTitle.setVisibility(View.GONE);
        }
        else {
            holder.newsTitle.setText(newsList.get(position).title);
            holder.newsTitle.setVisibility(View.VISIBLE);
            holder.newsTitle.setGravity(Gravity.LEFT);
        }

        if (newsList.get(position).annotation.equals("")) {
            holder.newsAnnotation.setVisibility(View.GONE);
        }
        else {
            holder.newsAnnotation.setText(newsList.get(position).annotation);
            holder.newsAnnotation.setVisibility(View.VISIBLE);
        }

        holder.newsImage.setVisibility(View.VISIBLE);
        holder.newsImage.setImageBitmap(null);
        imageLoader.loadBitmap(newsList.get(position).imageUrl, holder.newsImage);
    }

    private void bindTopNews(NewsViewHolder holder, int position) {
        holder.newsTopNewsContainer.setVisibility(View.VISIBLE);
        holder.newsNewsContainer.setVisibility(View.GONE);

        if (newsList.get(position).title.equals("")) {
            holder.newsTitleTopNews.setVisibility(View.GONE);
        }
        else {
            holder.newsTitleTopNews.setText(newsList.get(position).title);
            holder.newsTitleTopNews.setVisibility(View.VISIBLE);
            holder.newsTitleTopNews.setGravity(Gravity.LEFT);
        }

        if (newsList.get(position).annotation.equals("")) {
            holder.newsAnnotationTopNews.setVisibility(View.GONE);
        }
        else {
            holder.newsAnnotationTopNews.setText(newsList.get(position).annotation);
            holder.newsAnnotationTopNews.setVisibility(View.VISIBLE);
        }

        if (newsList.get(position).description.equals("")) {
            holder.newsDescriptionTopNews.setVisibility(View.GONE);
        }
        else {
            holder.newsDescriptionTopNews.setText(newsList.get(position).description);
            holder.newsDescriptionTopNews.setVisibility(View.VISIBLE);
        }

        holder.newsTopNewsImage.setVisibility(View.VISIBLE);
        holder.newsTopNewsImage.setImageBitmap(null);
        imageLoader.loadBitmap(newsList.get(position).imageUrl, holder.newsTopNewsImage);
    }

    private void bindHeader(NewsViewHolder holder) {
        holder.newsImage.setVisibility(View.GONE);
        holder.newsAnnotation.setVisibility(View.GONE);
        holder.newsDescriptionTopNews.setVisibility(View.GONE);

        holder.newsTitle.setVisibility(View.VISIBLE);
        holder.newsTitle.setGravity(Gravity.CENTER);
        holder.newsTitle.setText("Нажмите или потяните вниз, чтобы обновить");
    }

    @Override
    public void onItemClick(int position) {
        if (hasHeader && position == 0) {
            Log.d(TAG, "Click on the header");

            onItemClick.onHeaderClick();
        }
        else {
            Log.d(TAG, String.format("Select item = %s, NewsUrl = %s",
                    position, newsList.get(position).newsUrl));

            onItemClick.onItemClick(newsList.get(position).newsUrl);
        }
    }
}
