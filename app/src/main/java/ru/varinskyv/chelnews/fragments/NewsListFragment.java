package ru.varinskyv.chelnews.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ru.varinskyv.chelnews.NewsActivity;
import ru.varinskyv.chelnews.R;
import ru.varinskyv.chelnews.adapters.NewsAdapter;
import ru.varinskyv.chelnews.data.News;
import ru.varinskyv.chelnews.data.NewsTab;
import ru.varinskyv.chelnews.utils.Constants;
import ru.varinskyv.chelnews.utils.NewsLoader;

public class NewsListFragment extends Fragment implements NewsAdapter.OnItemClick, SwipeRefreshLayout.OnRefreshListener, NewsLoader.OnLoadNewsList {

    public static final String NEWS_TAB = "NEWS_TAB";
    public static final String TAB_POSITION = "TAB_POSITION";

    private ArrayList<News> newsList;
    private NewsAdapter newsAdapter;
    private Context context;
    private SwipeRefreshLayout newsSwipeRefresh;
    private int tabPosition;
    private NewsTab newsTab;
    private NewsLoader newsLoader;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_news_list, container, false);

        context = getActivity();

        Bundle args = getArguments();
        tabPosition = args.getInt(TAB_POSITION);
        newsTab = (NewsTab) args.getSerializable(NEWS_TAB);

        newsLoader = new NewsLoader(context);
        newsLoader.setOnLoadNewsListListener(this);
        newsList = newsLoader.newsListCache.getNewsList(tabPosition);
        newsAdapter = new NewsAdapter(context, tabPosition, newsList, this);

        RecyclerView newsListView = (RecyclerView) rootView.findViewById(R.id.main_news_list);
        newsListView.setLayoutManager(new LinearLayoutManager(context));
        newsListView.setAdapter(newsAdapter);

        newsSwipeRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.main_swipe_refresh);
        newsSwipeRefresh.setOnRefreshListener(this);
        newsSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                if (newsList.size() == 0){
                    newsSwipeRefresh.setRefreshing(true);
                    newsLoader.loadNewsList(tabPosition, newsTab);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onItemClick(String newsUrl) {
        Intent intent = new Intent(context, NewsActivity.class);
        intent.putExtra(Constants.NEWS_URL, newsUrl);
        intent.putExtra(Constants.CITY_URL, "/chel/");
        startActivity(intent);
    }

    @Override
    public void onHeaderClick() {
        updateNewsList();
    }

    @Override
    public void onRefresh() {
        updateNewsList();
    }

    @Override
    public void onNewsListLoadDone(ArrayList<News> result) {
        newsList.clear();
        newsList.addAll(0, result);

        newsAdapter.notifyDataSetChanged();

        newsSwipeRefresh.setRefreshing(false);
    }

    @Override
    public void onNewsListLoadError() {
        if (!newsAdapter.hasHeader) {
            newsList.add(0, new News(true));
            newsAdapter.notifyDataSetChanged();
        }

        newsSwipeRefresh.setRefreshing(false);
    }

    private void updateNewsList() {
        newsSwipeRefresh.setRefreshing(true);
        newsLoader.loadNewsList(tabPosition, newsTab);
    }
}
